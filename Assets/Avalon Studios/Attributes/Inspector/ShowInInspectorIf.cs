﻿using System;
using UnityEngine;

namespace AvalonStudios.Additions.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public sealed class ShowInInspectorIf : PropertyAttribute
    {
        public readonly string conditionalSourceField;

        public readonly bool showIf = false;

        public bool isIdented = false;

        public ShowInInspectorIf(string conditionalSourceField)
        {
            this.conditionalSourceField = conditionalSourceField;
            this.showIf = false;
        }
        
        public ShowInInspectorIf(string conditionalSourceField, bool showIf, bool isIdented = false)
        {
            this.conditionalSourceField = conditionalSourceField;
            this.showIf = showIf;
            this.isIdented = isIdented;
        }
    }
}
