﻿using AvalonStudios.Additions.Extensions;

using UnityEngine;
using UnityEditor;

namespace AvalonStudios.Additions.Attributes
{
    [CustomPropertyDrawer(typeof(ShowInInspectorIf))]
    public class ShowInInspectorPropertyDrawer : PropertyDrawer
    {
        private ShowInInspectorIf showInInspector;
        private bool enabledDisabled = false; // if this is true, then draw property
        private bool active;
        private string name;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (showInInspector == null) showInInspector = attribute as ShowInInspectorIf;
            bool enabled = GetConditionalHideAttribute(showInInspector, property);
            //Enable / Disable the property (field)
            bool wasEnabled = GUI.enabled;

            active = active == showInInspector.showIf;
            name = label.text.ContainsBackingField() ? property.displayName.RenameAutoProperty() : property.displayName;
            
            if (showInInspector.isIdented)
                EditorGUI.indentLevel++;
            //Check if we should draw the property
            if (enabled)
            {
                if (enabled == showInInspector.showIf)
                {
                    enabledDisabled = true;
                    EditorGUI.PropertyField(position, property, new GUIContent(name), true);
                }
                else
                    enabledDisabled = false;
            }
            else
            {
                if (enabled == showInInspector.showIf)
                {
                    enabledDisabled = true;
                    EditorGUI.PropertyField(position, property, new GUIContent(name), true);
                }
                else
                    enabledDisabled = false;
            }
            
            if (showInInspector.isIdented)
                EditorGUI.indentLevel--;

            GUI.enabled = wasEnabled;
        }

        private bool GetConditionalHideAttribute(ShowInInspectorIf condHAtt, SerializedProperty property)
        {
            bool enabled = true;
            // Look for the sourcefield within the object that the property belongs to
            string propertyPath = property.GetPropertyPath(); // Returns the property path of the property we want to apply the attribute to
            string conditionalPath = default;
            conditionalPath = propertyPath.Replace(property.GetName(),
                    condHAtt.conditionalSourceField); // Changes the path to the conditionalsource property path

            SerializedProperty sourcePropertyValue = property.serializedObject.FindSerializedProperty(conditionalPath);
            
            if (sourcePropertyValue == null)
            {
                conditionalPath = propertyPath.Replace(property.GetName(), condHAtt.conditionalSourceField.RenamePropertyToAutoProperty());
                sourcePropertyValue = property.serializedObject.FindSerializedProperty(conditionalPath);
            }
            
            if (sourcePropertyValue != null)
                enabled = sourcePropertyValue.boolValue;
            else
                Debug.LogWarning($"Attempting to use a ConditionalHideAttribute but no matching SourcePropertyValue found in object: {condHAtt.conditionalSourceField}",
                    property.serializedObject.targetObject);

            return enabled;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) => 
            enabledDisabled ? EditorGUI.GetPropertyHeight(property, label, true) : 0;
    }
}
