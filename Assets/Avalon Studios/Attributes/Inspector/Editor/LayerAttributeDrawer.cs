using AvalonStudios.Additions.Extensions;
using System;
using UnityEngine;
using UnityEditor;

namespace AvalonStudios.Additions.Attributes
{
     [CustomPropertyDrawer(typeof(LayerAttribute))]
    public class LayerAttributeDrawer : PropertyDrawer
    {
        private static readonly string ERROR_SERIALIZED_PROPERTY_TYPE = 
            string.Format("{0} only support serialized properties of type {1} ({2}), {3} ({4}), {5} ({6}) or {7} ({8})", 
                (object) typeof (LayerAttribute), (object) "Integer", (object) typeof (int), (object) "Float", 
                (object) typeof (float), (object) "String", (object) typeof (string), (object) "LayerMask", (object) typeof (LayerMask));

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            GUIContent name = new GUIContent(label.text.ContainsBackingField()
                ? property.displayName.RenameAutoProperty()
                : label.text);
            
            EditorGUI.BeginProperty(position, name, property);
            EditorGUI.BeginChangeCheck();
            int layer = EditorGUI.LayerField(position, name, property.intValue);
            if (EditorGUI.EndChangeCheck())
            {
                switch (property.propertyType)
                {
                    case SerializedPropertyType.Integer:
                        property.intValue = layer;
                        break;
                    case SerializedPropertyType.Float:
                        property.floatValue = (float) layer;
                        break;
                    case SerializedPropertyType.String:
                        property.stringValue = LayerMask.LayerToName(layer);
                        break;
                    case SerializedPropertyType.LayerMask:
                        property.intValue = layer;
                        break;
                    default:
                        throw new ArgumentException(LayerAttributeDrawer.ERROR_SERIALIZED_PROPERTY_TYPE);
                }
            }
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) => EditorGUI.GetPropertyHeight(property, label, true);
    }
}
