using AvalonStudios.Additions.Extensions;

using UnityEngine;
using UnityEditor;

namespace AvalonStudios.Additions.Attributes
{
    [CustomPropertyDrawer(typeof(ShowInInspectorIfEnumIs))]
    public class ShowInInspectorIfEnumIsDrawer : PropertyDrawer
    {
        private ShowInInspectorIfEnumIs propertyDrawer;

        private SerializedProperty sourceProperty;
        private GUIContent name;
        private bool isEnabled;
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (propertyDrawer == null) propertyDrawer = (ShowInInspectorIfEnumIs) attribute;
            int value = GetSourcePropertyAttributeValue(propertyDrawer, property, out sourceProperty);
            
            name = new GUIContent(label.text.ContainsBackingField() ? property.displayName.RenameAutoProperty() : property.displayName);

            if (value == propertyDrawer.enumValue)
            {
                isEnabled = true;
                EditorGUI.PropertyField(position, property, name, true);
            }
            else
                isEnabled = false;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) =>
            isEnabled ? EditorGUI.GetPropertyHeight(property, label, true) : 0;
        
        private int GetSourcePropertyAttributeValue(ShowInInspectorIfEnumIs attr, SerializedProperty property, out SerializedProperty sourceProperty)
        {
            int enumValue = -1;
            // Look for the sourcefield within the object that the property belongs to
            string propertyPath = property.propertyPath; // Returns the property path of the property we want to apply the attribute to
            string conditionalPath = propertyPath.Replace(property.name, attr.conditionalSourceField); // Changes the path to the conditionalsource property path
            sourceProperty = property.serializedObject.FindSerializedProperty(conditionalPath);
            if (sourceProperty == null)
            {
                conditionalPath = propertyPath.Replace(property.name, attr.conditionalSourceField.RenamePropertyToAutoProperty());
                sourceProperty = property.serializedObject.FindSerializedProperty(conditionalPath);
            }

            if (sourceProperty != null)
                enumValue = sourceProperty.enumValueIndex;
            else
                Debug.LogWarning($"Attempting to use a ConditionalHideAttribute but no matching SourcePropertyValue found in object: {attr.conditionalSourceField}",
                    property.serializedObject.targetObject);

            return enumValue;
        }
    }
}
