using System;

using UnityEngine;

namespace AvalonStudios.Additions.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public class LayerAttribute : PropertyAttribute
    {
    }
}
