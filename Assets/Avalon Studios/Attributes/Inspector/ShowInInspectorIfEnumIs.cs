using System;

using UnityEngine;

namespace AvalonStudios.Additions.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, Inherited = true, AllowMultiple = true)]
    public sealed class ShowInInspectorIfEnumIs : PropertyAttribute
    {
        public readonly string conditionalSourceField;
        public readonly int enumValue;
        
        public ShowInInspectorIfEnumIs(string conditionalSourceField, int enumValue)
        {
            this.conditionalSourceField = conditionalSourceField;
            this.enumValue = enumValue;
        }
    }
}
