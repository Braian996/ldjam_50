﻿using AvalonStudios.Additions.Extensions;
using AvalonStudios.Additions.Attributes.StylizedGUIs;
using AvalonStudios.Additions.Utils.EditorHandle;
using AvalonStudios.Additions.Utils.IconContentHandle;

using System;
using System.Reflection;

using UnityEngine;
using UnityEditor;
using Object = UnityEngine.Object;

namespace AvalonStudios.Additions.Tools.TransformEditorExtension
{
    [CustomEditor(typeof(Transform)), CanEditMultipleObjects]
    public class TransformEditorExtension : Editor
    {
        private enum AlignToType { lastSelected, firstSelected }
        private enum AxisFlag { X = 1, Y = 2, Z = 4 }

        private class Properties
        {
            public SerializedProperty Position { get; set; }
            public SerializedProperty Rotation { get; set; }
            public SerializedProperty EulerAngles { get; set; }
            public SerializedProperty Scale { get; set; }

            public Properties(SerializedObject serializedObject)
            {
                Position = serializedObject.FindProperty("m_LocalPosition");
                Rotation = serializedObject.FindProperty("m_LocalRotation");
                EulerAngles = serializedObject.FindProperty("m_LocalEulerAnglesHint");
                Scale = serializedObject.FindProperty("m_LocalScale");
            }
        }

        private class Content
        {
            // Tool button sizes
            public static readonly GUILayoutOption ToolButtonWidth = GUILayout.Width(40);
            public static readonly GUILayoutOption ToolButtonHeight = GUILayout.Height(20);
            public static readonly GUILayoutOption ResetButtonWidth = GUILayout.Width(250);
            // ---

            // Label sizes
            public static readonly GUILayoutOption LabelScene = GUILayout.Width(75);

            public static readonly Color ContainerColor = EditorGUIUtility.isProSkin ? Color.green : Color.yellow;

            public static readonly GUIContent None = GUIContent.none;

            public static readonly GUIContent Position = GUIContentIcon(IconContentHandle.MoveTool.image, "Move tool");
            public static readonly GUIContent Rotation = GUIContentIcon(IconContentHandle.RotateTool.image, "Rotate tool");
            public static readonly GUIContent Scale = GUIContentIcon(IconContentHandle.ScaleTool.image, "Scale tool");
            public static readonly GUIContent Unified = GUIContentIcon(IconContentHandle.TransformTool.image, "Move, Rotate or Scale selected objects");
            public static readonly GUIContent Quaternion = new GUIContent("Quaternion", "The local quaternion rotation of this Game Object relative to the parent.");
            public static readonly GUIContent MoveSnap = new GUIContent("Move", "Grid spacing.");
            public static readonly GUIContent ScaleSnap = new GUIContent("Scale", "Grid spacing for scaling.");
            public static readonly GUIContent RotationSnap = new GUIContent("Rotation", "Grid spacing for rotation in degrees.");

            public static GUIContent GUIContentTextWithIcon(string label, Texture icon, string tooltip = "") => new GUIContent(label, icon, $"{tooltip}");

            public static GUIContent GUIContentIcon(Texture icon, string tooltip = "") => new GUIContent(icon, tooltip);
        }

        private static AlignToType alignTo = AlignToType.lastSelected;
        private static AxisFlag alignmentAxis = AxisFlag.X;

        private Editor defaultEditor;
        private Transform transform;
        private Properties properties;

        private static int indexToolbarTransformTools = 0;

        private static bool quaternionFoldout;
        private static bool worldTransform;

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            ChangeToolSelected();
            defaultEditor.OnInspectorGUI();
            DrawQuaternionInspector();
            WorldTransformInspector();
            ToolTransformInspector();
            InfoObjectInspector();
            SnapToolsInspector();
            DrawCommandsInspector();
            serializedObject.ApplyModifiedProperties();
        }

        private void OnSceneGUI()
        {
            ChangeToolSelected();

            if (!(Selection.transforms.Length > 1))
            {
                Handles.BeginGUI();
                QuickTools();
                Handles.EndGUI();
            }
        }

        private void QuickTools()
        {
            GUI.color = Color.yellow;
            EditorGUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.Width(200));
            GUI.color = Color.white;

            EditorGUILayout.LabelField("Transform", GUIStylesConstants.TitleStyle(Color.black));
            GUILayout.Space(5);

            GUIStyle styleButton = new GUIStyle(GUI.skin.label);
            styleButton.fontStyle = FontStyle.Bold;

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Position:", styleButton, Content.LabelScene);
            GUI.enabled = false;
            EditorGUILayout.Vector3Field("", transform.localPosition);
            GUI.enabled = true;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Rotation:", styleButton, Content.LabelScene);
            GUI.enabled = false;
            EditorGUILayout.Vector3Field("", transform.localRotation.eulerAngles);
            GUI.enabled = true;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Scale:", styleButton, Content.LabelScene);
            GUI.enabled = false;
            EditorGUILayout.Vector3Field("", transform.localScale);
            GUI.enabled = true;
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Focus"))
            {
                Undo.RecordObject(transform, "Focus");
                SceneView.lastActiveSceneView.FrameSelected();
            }

            if (GUILayout.Button("Duplicate"))
            {
                Undo.RecordObject(transform, "Duplicate Object");
                Instantiate(transform.gameObject, transform.localPosition, Quaternion.identity);
            }

            if (GUILayout.Button("Delete"))
            {
                Undo.RecordObject(transform, "Delete");
                DestroyImmediate(transform.gameObject);
            }

            EditorGUILayout.EndVertical();
        }

        private void DrawQuaternionInspector()
        {
            quaternionFoldout = EditorGUILayout.Foldout(quaternionFoldout, $" Quaternion Rotation:    {transform.localRotation.ToString("F3")}");
            if (quaternionFoldout)
            {
                EditorGUI.indentLevel++;
                Rect rect = EditorHandle.GetRectOfInspector;
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(properties.Rotation, Content.Quaternion, true);
                if (properties.Rotation.isExpanded)
                {
                    rect.height = EditorGUIUtility.singleLineHeight;
                    rect.xMin += EditorGUIUtility.labelWidth;
                    EditorGUI.HelpBox(rect, "Editing quaternions manually is inadvisable.", MessageType.Warning);
                }
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(transform, $"Modify quaternion rotation on {transform.name}");
                    transform.localRotation = properties.Rotation.quaternionValue;
                }
                EditorGUI.indentLevel--;
            }
        }

        private void WorldTransformInspector()
        {
            worldTransform = EditorGUILayout.Foldout(worldTransform, Content.GUIContentTextWithIcon(" World Transform Space", IconContentHandle.TransformIcon.image));
            if (worldTransform)
            {
                EditorGUI.indentLevel++;
                GUI.enabled = false;
                EditorGUILayout.Vector3Field("Position", transform.position);
                EditorGUILayout.Vector3Field("Rotaion", transform.eulerAngles);
                EditorGUILayout.Vector3Field("Scale", transform.lossyScale);
                EditorGUILayout.Vector4Field("Quaternion", transform.rotation.ToVector4());
                GUI.enabled = true;
                EditorGUI.indentLevel--;
            }
        }

        private void ToolTransformInspector()
        {
            Rect rect = EditorHandle.GetRectOfInspector;
            StylizedGUI.DrawInspectorHeader(rect, "Transform Tools", displacementY: 5);
            GUILayout.Space((rect.xMax + StylizedGUI.height) + 25);

            indexToolbarTransformTools = GUILayout.Toolbar(indexToolbarTransformTools, new GUIContent[] { Content.Position, Content.Rotation, Content.Scale, Content.Unified });
            if (indexToolbarTransformTools == 0)
            {
                Undo.RecordObject(transform, "Move Tool Selected");
                UnityEditor.Tools.current = Tool.Move;
            }

            if (indexToolbarTransformTools == 1)
            {
                Undo.RecordObject(transform, "Rotate Tool Selected");
                UnityEditor.Tools.current = Tool.Rotate;
            }

            if (indexToolbarTransformTools == 2)
            {
                Undo.RecordObject(transform, "Scale Tool Selected");
                UnityEditor.Tools.current = Tool.Scale;
            }
            if (indexToolbarTransformTools == 3)
            {
                Undo.RecordObject(transform, "Unified Tool Selected");
                UnityEditor.Tools.current = Tool.Transform;
            }
            GUILayout.Space(5);

            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            GUILayout.Label(Content.GUIContentIcon(IconContentHandle.InfoIcon.image));

            EditorGUILayout.BeginVertical();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label(Content.GUIContentTextWithIcon("Q (Hand Tool)", IconContentHandle.ViewToolMove.image));
            GUILayout.Label(Content.GUIContentTextWithIcon("W (Move Tool)", IconContentHandle.MoveTool.image));
            GUILayout.Label(Content.GUIContentTextWithIcon("E (Rotate Tool)", IconContentHandle.RotateTool.image));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label(Content.GUIContentTextWithIcon("R (Scale Tool)", IconContentHandle.ScaleTool.image));
            GUILayout.Label(Content.GUIContentTextWithIcon("T (Rect Tool)", IconContentHandle.RectTool.image));
            GUILayout.Label(Content.GUIContentTextWithIcon("Y (Unified Tool)", IconContentHandle.TransformTool.image));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();

            EditorGUILayout.EndHorizontal();
        }

        private void SnapToolsInspector()
        {
            Rect rect = EditorHandle.GetRectOfInspector;
            StylizedGUI.DrawInspectorHeader(rect, "Snap Tools", displacementY: 3);
            GUILayout.Space((rect.xMax + StylizedGUI.height) + 25);
            EditorGUILayout.HelpBox("Hold down Ctrl(PC) or CMD(Mac) to activate Snap Tool Object in the Move, Rotate or Scale tools.\n" +
                "Hold down V to snap object by vertex.\n" +
                "To edit the grid and snap settings go to:\n" + 
                "Edit > Grid and Snap Settings", MessageType.Info);
            GUILayout.Space(5);

            GUIStyle snapStyle = new GUIStyle(GUI.skin.GetStyle("HelpBox"));
            snapStyle.padding = new RectOffset(snapStyle.padding.left, snapStyle.padding.right, snapStyle.padding.top + 2, snapStyle.padding.bottom + 2);
            GUILayout.BeginVertical(snapStyle);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Snap Up"))
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, Vector3.up, out hit))
                {
                    Undo.RecordObject(transform, "Snap Up");
                    transform.position = hit.point;
                }
            }
            if (GUILayout.Button("Snap Down"))
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, Vector3.down, out hit))
                {
                    Undo.RecordObject(transform, "Snap Down");
                    transform.position = hit.point;
                }
            }
            if (GUILayout.Button("Snap Right"))
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, Vector3.right, out hit))
                {
                    Undo.RecordObject(transform, "Snap Right");
                    transform.position = hit.point;
                }
            }
            if (GUILayout.Button("Snap Left"))
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, Vector3.left, out hit))
                {
                    Undo.RecordObject(transform, "Snap Left");
                    transform.position = hit.point;
                }
            }
            GUILayout.EndHorizontal();

            AlignmentSceneInspector();
            GUILayout.EndVertical();
        }

        private void AlignmentSceneInspector()
        {
            EditorGUILayout.LabelField("Alignment", EditorStyles.boldLabel);
            alignTo = (AlignToType)EditorGUILayout.EnumPopup("Align to", alignTo);
            alignmentAxis = (AxisFlag)EditorGUILayout.EnumFlagsField("Axis", alignmentAxis);

            string buttonLabel = "Select Another Object to Align to";
            bool enableButton = false;
            Transform[] selectedTransforms = Selection.transforms;
            if (selectedTransforms.Length > 1)
            {
                if (alignTo == AlignToType.lastSelected)
                    buttonLabel = "Align to " + selectedTransforms[selectedTransforms.Length - 1].name;
                else
                    buttonLabel = "Align to " + selectedTransforms[0].name;
                enableButton = true;
            }

            GUI.enabled = enableButton;
            if (GUILayout.Button(buttonLabel))
                AlignTo(alignTo, alignmentAxis);
            GUI.enabled = true;
        }

        private void AlignTo(AlignToType to, AxisFlag axis)
        {
            Transform[] selectedTransforms = Selection.transforms;

            int targetIndex = 0;
            if (to == AlignToType.lastSelected)
                targetIndex = selectedTransforms.Length - 1;

            for (int i = 0; i < selectedTransforms.Length; i++)
            {
                if (i == targetIndex)
                    continue;

                Vector3 temp = selectedTransforms[i].position;

                if ((axis & AxisFlag.X) == AxisFlag.X)
                    temp.x = selectedTransforms[targetIndex].position.x;

                if ((axis & AxisFlag.Y) == AxisFlag.Y)
                    temp.y = selectedTransforms[targetIndex].position.y;

                if ((axis & AxisFlag.Z) == AxisFlag.Z)
                    temp.z = selectedTransforms[targetIndex].position.z;

                Undo.RecordObject(selectedTransforms[i], selectedTransforms[i].name + " aligned to " + selectedTransforms[targetIndex].name);
                selectedTransforms[i].position = temp;
            }
        }

        private void InfoObjectInspector()
        {
            GUILayout.Space(5);
            string rootInfo = "----";
            string parentInfo = "----";
            if (Selection.transforms.Length == 1)
            {
                rootInfo = transform.root.GetInstanceID() == transform.GetInstanceID() ? "Itself" : transform.root.name;
                parentInfo = transform.parent.IsNull() ? "None" : transform.parent.name;
            }

            GUI.color = Content.ContainerColor;
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            GUI.color = Color.white;
            EditorGUILayout.LabelField("Object Info:");
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(Content.GUIContentTextWithIcon($"Root: {rootInfo}", IconContentHandle.TransformIcon.image));
            EditorGUILayout.LabelField(Content.GUIContentTextWithIcon($"Parent: {parentInfo}", IconContentHandle.TransformIcon.image));
            EditorGUILayout.EndHorizontal();
            GUILayout.Space(3);

            GUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(transform.parent.IsNull());
            if (GUILayout.Button("Unparent"))
            {
                Undo.RecordObject(transform, "SetParent");
                foreach (Object obj in targets)
                {
                    Transform t = (Transform)obj;
                    if (!t.parent.transform.parent.IsNull())
                        t.SetParent(t.parent.transform.parent);
                    else
                        t.SetParent(null);
                }
            }
            EditorGUI.EndDisabledGroup();
            GUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
        }

        private void DrawCommandsInspector()
        {
            Rect rect = EditorHandle.GetRectOfInspector;
            StylizedGUI.DrawInspectorHeader(rect, "Helpers", displacementY: 3);
            GUILayout.Space((rect.xMax + StylizedGUI.height) + 25);

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            GUILayout.Space(3);

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Reset Position"))
            {
                Undo.RecordObject(transform, "Reset Position");
                transform.localPosition = Vector3.zero;
            }
            if (GUILayout.Button("Reset Rotation"))
            {
                Undo.RecordObject(transform, "Reset Rotation");
                transform.localRotation = Quaternion.Euler(Vector3.zero);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Reset Scale"))
            {
                Undo.RecordObject(transform, "Reset Scale");
                transform.localScale = Vector3.one;
            }
            if (GUILayout.Button("Reset Transform"))
            {
                Undo.RecordObject(transform, "Reset All");
                transform.localPosition = Vector3.zero;
                transform.localRotation = Quaternion.Euler(Vector3.zero);
                transform.localScale = Vector3.one;
            }
            GUILayout.EndHorizontal();

            GUILayout.Space(3);
            EditorGUILayout.EndVertical();
        }

        private void ChangeToolSelected()
        {
            switch (UnityEditor.Tools.current)
            {
                case Tool.Move:
                    indexToolbarTransformTools = 0;
                    break;
                case Tool.Rotate:
                    indexToolbarTransformTools = 1;
                    break;
                case Tool.Scale:
                    indexToolbarTransformTools = 2;
                    break;
                case Tool.Transform:
                    indexToolbarTransformTools = 3;
                    break;
                default:
                    indexToolbarTransformTools = 4;
                    break;
            }
        }

        private void OnEnable()
        {
            properties = new Properties(serializedObject);
            defaultEditor = CreateEditor(targets, Type.GetType("UnityEditor.TransformInspector, UnityEditor"));
            transform = (Transform)target;
            ChangeToolSelected();
        }

        private void OnDisable()
        {
            MethodInfo disableMethod = defaultEditor.GetType().GetMethod("OnDisable", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            if (disableMethod != null)
                disableMethod.Invoke(defaultEditor, null);
            DestroyImmediate(defaultEditor);
        }
    }
}
