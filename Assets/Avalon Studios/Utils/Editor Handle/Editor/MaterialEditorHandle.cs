﻿using UnityEngine;
using UnityEditor;

namespace AvalonStudios.Additions.Utils.MaterialEditorHandle
{
    public sealed class MaterialEditorHandle
    {
        public static void DrawMaterialProperty(Rect position, MaterialProperty prop, string label, MaterialEditor editor)
        {
            switch (prop.type)
            {
                case MaterialProperty.PropType.Range:
                    prop.floatValue = editor.RangeProperty(position, prop, label);
                    break;
                case MaterialProperty.PropType.Float:
                    prop.floatValue = editor.FloatProperty(position, prop, label);
                    break;
                case MaterialProperty.PropType.Color:
                    prop.colorValue = editor.ColorProperty(position, prop, label);
                    break;
                case MaterialProperty.PropType.Texture:
                    prop.textureValue = editor.TextureProperty(position, prop, label);
                    break;
                case MaterialProperty.PropType.Vector:
                    prop.vectorValue = editor.VectorProperty(position, prop, label);
                    break;
            }
        }
    }
}