﻿using UnityEngine;

namespace AvalonStudios.Additions.Utils.VectorHandle
{
    public class VectorHandle
    {
        /// <summary>
        /// Set a general value to the <seealso cref="Vector2"/>.
        /// </summary>
        /// <param name="value">Value to set the <seealso cref="Vector2"/>.</param>
        /// <returns>Returns a <seealso cref="Vector2"/>.</returns>
        public static Vector2 SetVector2(float value) => SetVector(value, value);
        
        /// <summary>
        /// Set values to the <seealso cref="Vector2"/>.
        /// </summary>
        /// <returns>Returns a <seealso cref="Vector2"/>.</returns>
        public static Vector2 SetVector(float x, float y) => new Vector2(x, y);
        
        /// <summary>
        /// Set a general value to the <seealso cref="Vector3"/>.
        /// </summary>
        /// <param name="value">Value to set the <seealso cref="Vector3"/>.</param>
        /// <returns>Returns a <seealso cref="Vector3"/>.</returns>
        public static Vector3 SetVector3(float value) => SetVector(value, value, value);

        /// <summary>
        /// Set values to the <seealso cref="Vector3"/>.
        /// </summary>
        /// <returns>Returns a <seealso cref="Vector3"/>.</returns>
        public static Vector3 SetVector(float x, float y, float z) => new Vector3(x, y, z);
        
        /// <summary>
        /// Set a general value to the <seealso cref="Vector4"/>.
        /// </summary>
        /// <param name="value">Value to set the <seealso cref="Vector4"/>.</param>
        /// <returns>Returns a <seealso cref="Vector4"/>.</returns>
        public static Vector4 SetVector4(float value) => SetVector(value, value, value, value);
        
        /// <summary>
        /// Set values to the <seealso cref="Vector4"/>.
        /// </summary>
        /// <returns>Returns a <seealso cref="Vector4"/>.</returns>
        public static Vector4 SetVector(float x, float y, float z, float w) => new Vector4(x, y, z, w);
    }
}