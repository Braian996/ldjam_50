﻿using System.Linq;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;
using UnityEngine.Rendering;

[InitializeOnLoad]
public class RenderingPipelineDefines
{
	private const string HDR_PIPELINE = "HDRenderPipelineAsset";
	private const string UR_PIPELINE = "UniversalRenderPipelineAsset";
	private const string LWR_PIPELINE = "LightweightRenderPipelineAsset";

	private const string HDRP_DIRECTIVE = "UNITY_PIPELINE_HDRP";
	private const string URP_DIRECTIVE = "UNITY_PIPELINE_URP";
	
	private enum PipelineType
	{
		Unsupported,
		BuiltInPipeline,
		UniversalPipeline,
		HDPipeline
	}

	static RenderingPipelineDefines()
	{
		UpdatePipelines();
	}

	/// <summary>
	/// Update the unity pipeline defines for URP
	/// </summary>
	private static void UpdatePipelines()
	{
		PipelineType pipelineType = GetPipeline();

		if (pipelineType == PipelineType.UniversalPipeline)
		{
			AddDefine(URP_DIRECTIVE);
			RemoveDefine(HDRP_DIRECTIVE);
		}

		if (pipelineType == PipelineType.HDPipeline)
		{
			AddDefine(HDRP_DIRECTIVE);
			RemoveDefine(URP_DIRECTIVE);
		}
	}

	/// <summary>
	/// Returns the type of render pipeline that is currently running.
	/// </summary>
	/// <returns><seealso cref="PipelineType"/></returns>
	private static PipelineType GetPipeline()
	{
#if UNITY_2019_1_OR_NEWER
		if (GraphicsSettings.renderPipelineAsset != null)
		{
			// SRP
			var srpType = GraphicsSettings.renderPipelineAsset.GetType().ToString();
			
			if (srpType.Contains(HDR_PIPELINE))
				return PipelineType.HDPipeline;
			
			if (srpType.Contains(UR_PIPELINE) || srpType.Contains(LWR_PIPELINE))
				return PipelineType.UniversalPipeline;
			
			return PipelineType.Unsupported;
		}
#elif UNITY_2017_1_OR_NEWER
		// SRP not supported before 2019
		if (GraphicsSettings.renderPipelineAsset != null) return PipelineType.Unsupported;
#endif
		// No SRP
		return PipelineType.BuiltInPipeline;
	}
	
	/// <summary>
	/// Add a custom define
	/// </summary>
	/// <param name="define">The define string.</param>
	private static void AddDefine(string define)
	{
		var definesList = GetDefines();
		if (!definesList.Contains(define))
		{
			definesList.Add(define);
			SetDefines(definesList);
		}
	}

	/// <summary>
	/// Remove a custom define
	/// </summary>
	/// <param name="define">The define string.</param>
	public static void RemoveDefine(string define)
	{
		var definesList = GetDefines();
		if (definesList.Contains(define))
		{
			definesList.Remove(define);
			SetDefines(definesList);
		}
	}

	public static List<string> GetDefines()
	{
		BuildTarget target = EditorUserBuildSettings.activeBuildTarget;
		BuildTargetGroup buildTargetGroup = BuildPipeline.GetBuildTargetGroup(target);
		string defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup);
		return defines.Split(';').ToList();
	}

	public static void SetDefines(List<string> defineList)
	{
		BuildTarget target = EditorUserBuildSettings.activeBuildTarget;
		BuildTargetGroup buildTargetGroup = BuildPipeline.GetBuildTargetGroup(target);
		string defines = string.Join(";", defineList.ToArray());
		PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, defines);
	}
}
