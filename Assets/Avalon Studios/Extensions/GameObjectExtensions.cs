using UnityEngine;

namespace AvalonStudios.Additions.Extensions
{
    public static class GameObjectExtensions
    {
        /// <summary>
        ///     <para>ActivatesDeactivates the GameObjects, depending on the given true or false/ value.</para>
        /// </summary>
        /// <param name="value">Activate or deactivate the objects, where true activates the GameObjects and false deactivates the GameObjects.</param>
        public static void SetActive(this GameObject[] gameObjects, bool value)
        {
            for (int i = 0; i < gameObjects.Length; i++)
                gameObjects[i].SetActive(value);
        }

        /// <summary>
        /// Set the layer to the GameObjects.
        /// </summary>
        /// <param name="layer">Layer value.</param>
        public static void SetLayer(this GameObject[] gameObjects, int layer)
        {
            for (int i = 0; i < gameObjects.Length; i++)
                gameObjects[i].layer = layer;
        }

        /// <summary>
        /// Set the layer to the GameObjects.
        /// </summary>
        /// <param name="layer">Layer name.</param>
        public static void SetLayer(this GameObject[] gameObjects, string layer)
        {
            for (int i = 0; i < gameObjects.Length; i++)
                gameObjects[i].layer = LayerMask.NameToLayer(layer);
        }

        /// <summary>
        /// Set the tag to the GameObjects.
        /// </summary>
        /// <param name="name">Tag's name.</param>
        public static void SetTag(this GameObject[] gameObjects, string name)
        {
            for (int i = 0; i < gameObjects.Length; i++)
                gameObjects[i].tag = name;
        }
    }
}
