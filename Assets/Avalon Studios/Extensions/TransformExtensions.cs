using UnityEngine;

namespace AvalonStudios.Additions.Extensions
{
    public static class TransformExtensions
    {
        /// <summary>
        /// Only use in 90, 270, -90, -270 rotation in Y
        /// </summary>
        /// <returns></returns>
        public static bool IsFlipX(this Transform transform)
        {
            float value = transform.eulerAngles.y;
            if (value == 90f || value == -270)
                return true;
            if (value == -90 || value == 270)
                return false;
            return false;
        }
    }
}
