﻿using UnityEngine;

namespace AvalonStudios.Additions.Extensions
{
    public static class LayerMaskExtensions
    {
        /// <summary>
        /// Convert a <see cref="LayerMask"/> value to it's true layer value.<br/>
        /// This should only be used if the <paramref name="layerMask"/> has a single layer.
        /// </summary>
        /// <param name="layerMask"><see cref="LayerMask"/></param>
        /// <returns>int layer number.</returns>
        public static int ToLayer(this LayerMask layerMask)
            // https://forum.unity.com/threads/get-the-layernumber-from-a-layermask.114553/#post-3021162
        {
            int bitMask = layerMask.value;
            int result = bitMask > 0 ? 0 : 31;
            while (bitMask > 1)
            {
                bitMask >>= 1;
                result++;
            }
            return result;
        }

        public static LayerMask ToLayerMaskValue(this int value) => 1 << value;

        /// <summary>
        /// Returns true if the <seealso cref="LayerMask"/> equals the layer of the <seealso cref="GameObject"/>.
        /// Returns false if not <seealso cref="LayerMask"/> was found.
        /// </summary>
        /// <param name="gameObject"></param>
        /// <param name="layerMask">The <seealso cref="LayerMask"/> to compare with the layer of the <seealso cref="GameObject"/></param>
        /// <returns>A <seealso cref="bool"/> result.</returns>
        public static bool MatchLayer(this GameObject gameObject, LayerMask layerMask)
        {
            for (int i = 0; i < 32; i++)
            {
                if (layerMask == (layerMask | (1 << i)))
                {
                    if (gameObject.layer == i)
                        return true;
                }
            }

            return false;
        }
        
        /// <summary>
        /// Assign a the parent game object layer to his childs.
        /// </summary>
        public static void AssignLayerToChilds(this GameObject gameObject)
        {
            int layer = gameObject.layer;
            
            if (gameObject.transform.childCount == 0)
                return;
            
            foreach (Transform child in gameObject.transform)
                child.gameObject.layer = layer;
        }
    }
}
