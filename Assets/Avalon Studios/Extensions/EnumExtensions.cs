using System;
using System.ComponentModel;
using System.Reflection;
using UnityEngine;

namespace AvalonStudios.Additions.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDescription(this Enum value)
        // https://stackoverflow.com/a/1415187
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (!name.Equals(""))
            {
                FieldInfo fieldInfo = type.GetField(name);
                if (fieldInfo != null)
                {
                    DescriptionAttribute attr =
                        (DescriptionAttribute) Attribute.GetCustomAttribute(fieldInfo, typeof(DescriptionAttribute));
                    if (attr != null)
                        return attr.Description;
                }
            }

            return "";
        }
    }
}
