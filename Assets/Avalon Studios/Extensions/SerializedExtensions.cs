using System.Collections;

using UnityEngine;
using UnityEditor;

namespace AvalonStudios.Additions.Extensions
{
    public static class SerializedExtensions
    {
#if UNITY_EDITOR
        /// <summary>
        /// Get the real property.
        /// </summary>
        /// <returns>Return the real serialized property.</returns>
        public static SerializedProperty GetProperty(this SerializedProperty property)
        {
            string[] elements = property.propertyPath.Split('.');
            SerializedProperty prop = property.serializedObject.FindSerializedProperty(elements[0]);

            return prop;
        }
        
        /// <summary>
        /// Name of the property.
        /// </summary>
        /// <returns>Type: <seealso cref="string"/>. Name of the property</returns>
        public static string GetName(this SerializedProperty property) => property.GetProperty().name;

        /// <summary>
        /// Get the property path.
        /// </summary>
        /// <returns>Full path of the property.</returns>
        public static string GetPropertyPath(this SerializedProperty property) => property.GetProperty().propertyPath;
        
        /// <summary>
        /// Find <seealso cref="SerializedProperty"/> by name.
        /// </summary>
        /// <param name="path">The path of the property.</param>
        /// <returns>
        /// This returns the <seealso cref="SerializedProperty"/> that matches with the specified path.
        /// </returns>
        public static SerializedProperty FindSerializedProperty(this SerializedObject serializedObject, string path)
        {
            SerializedProperty property = serializedObject.FindProperty(path);
            if (property.IsNull())
                property = serializedObject.FindProperty($"<{path}>k__BackingField");
            return property;
        }

        /// <summary>
        /// Retrieves the <seealso cref="SerializedProperty"/> at a relative path to the current property.
        /// </summary>
        /// <param name="path">The path of the property.</param>
        /// <returns>
        /// This returns the relative <seealso cref="SerializedProperty"/> that matches with the specified path.
        /// </returns>
        public static SerializedProperty FindSerializedPropertyRelative(this SerializedProperty serializedObject, string path)
        {
            SerializedProperty property = serializedObject.FindPropertyRelative(path);
            if (property.IsNull())
                property = serializedObject.FindPropertyRelative($"<{path}>k__BackingField");

            return property;
        }

        /// <summary>
        /// Find Auto <seealso cref="SerializedProperty"/> by name.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>
        /// This returns the Auto <seealso cref="SerializedProperty"/> that matches with the specified name of the property.
        /// </returns>
        public static SerializedProperty FindPropertyByAutoSerializePropertyName(this SerializedObject serializedObject, string propertyName) =>
            serializedObject.FindProperty($"<{propertyName}>k__BackingField");

        /// <summary>
        /// Retrieves the Auto <seealso cref="SerializedProperty"/> at a relative path to the current property.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>
        /// This returns the relative Auto <seealso cref="SerializedProperty"/> that matches with the specified name of the property.
        /// </returns>
        public static SerializedProperty FindPropertyRelativeByAutoSerializePropertyName(this SerializedProperty serializedObject, string propertyName) =>
            serializedObject.FindPropertyRelative($"<{propertyName}>k__BackingField");
#endif
    }
}
